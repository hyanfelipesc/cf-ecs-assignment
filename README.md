# cf-ecs-assignment

## Name
cf-ecs-assignment infrastructure

## Description
Repository was created in order to deploy the infrastructure to support the project 'Assignment'. \
2 repositories make up this project the cf-ecs-assignmment (https://gitlab.com/hyanfelipesc/cf-ecs-assignment) and the container-assignment (https://gitlab.com/hyanfelipesc/container-assignment)


## Installation

To deploy the Cloudformation you just need to commit on the branch main after that the pipeline is going to start running. \
You can also run the pipeline manually

![parameter image](images/Run.png)

This project is using the creentials as variables, the keys can be checked on the CI/CD settings variables 

![Variable image](images/variables.png)

## About the cloudformation
 The cloudformation is creating a ECS environment (Cluster, roles, security groups, loadbalancer, repository, taskdefinition, etc ..) \
 But is using the VPC/Subnet that were already created. \
 The VPC/Subnets are declared on the parameters from the Cloudformation template. \
 If necessary to deploy on another account please replace the VPC/Subnets ID

![parameter image](images/Parameters.png)


After the execution you can check the cloudformation stack on the AWS Cloudformation service

![parameter image](images/cloudformation.png)


After create the infraestructure its necessary to deploy the aplication, we created the Project container-assignment for it, please take a look on https://gitlab.com/hyanfelipesc/container-assignment


To acess the EC2 we created a bootstrap script to install the Session manager, so its not necessary to use a key to ssh into the instance, you can follow the steps bellow.

![parameter image](images/ec2-connect-1.png)

![parameter image](images/ec2-connect-2.png)

![parameter image](images/ec2-connect-3.png)


## Architecture

![parameter image](images/Architecture.png)


