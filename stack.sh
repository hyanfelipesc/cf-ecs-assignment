#/bin/bash
STACK=cluster-ecs
STATUS=$(aws cloudformation describe-stacks --stack-name $STACK 2> /dev/null | grep -i stackstatus | awk {'print $2'} | tr -d '",')
if
	### Check if the stack exist and create it
	[ -z $STATUS ]; then
		aws cloudformation create-stack \
			--stack-name $STACK \
			--template-body file://cluster-ecs.yml \
			--capabilities CAPABILITY_NAMED_IAM
fi
